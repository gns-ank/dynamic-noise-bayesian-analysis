# General options
# samplerate    : int
#   samplerate with which the data was sampled
# length        : double
#   The length of a subsample
# samples       : list of ints
#   subsamples to take to the right and left.
[model]
samplerate  = 4000
length      = 1
samples     = 100,2,1

# Signal modeling options
# amplitude :
#   The amplitude of the signal wavepacket
# frequency :
#   The frequency of the carrier wave
# width     :
#   The width of the gaussian envelope
# tcentral  :
#   The central time, at which the gaussian is at its peak
# phase     :
#   The relative phase of the carrier wave w.r.t the central time
[params]
amplitude   = 2e-5
frequency   = 400
width       = 0.008
tcentral    = 0.01
phase       = 0

# Noise properties in the generated signal
# amplitude :
#   The amplitude of the generated noise
# drift     :
#   The linear drift parameter, by how much the noise is increased in the last segment.
#   e.g. to increase noise 10% during the whole observation, use 0.1, for 20%, 0.2 and so on.
[noise]
amplitude   = 1e-18
drift       = 1.5

# Settings for file plotting. These can be changed, if we want different extensions or something.
#
# ext       : str
#   Filetype of the output plots (with/without the leading dot)
# params    : kwargs
#   The string will be interpreted as kwargs for the savefig function.
[plot]
ext         = png
params      = dpi=200

# Setting for directories
#
# plot      : str
#   Plotting output directory
[directories]
plot        = plots
