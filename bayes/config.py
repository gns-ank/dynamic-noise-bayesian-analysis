"""
A config parser to read configuration files.

Each type of config has a function and it return a dictionary.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

from configparser import ConfigParser
from os.path import expanduser


def insert(cfg, dictionary, key1, key2, keytype):
    """
    A function to properly set a type of a configuration file
    """

    # Create an empty dict
    if key1 not in list(dictionary.keys()):
        dictionary[key1] = {}

    # Should we iterate over the whole lot of keys?
    if key2 is None:
        dictionary[key1] = {k: keytype(v) for k, v in cfg[key1].items()}
    else:
        if keytype == bool:
            dictionary[key1][key2] = cfg[key1].getboolean(key2)
        else:
            dictionary[key1][key2] = keytype(cfg[key1][key2])


def data(filename, out=None):
    """
    Read a config file for a data generator.

    This will ensure that the types are what I expect them to be.
    """
    if out is None:
        out = {}
    elif type(out) != dict:
        raise Exception("The output needs to be a dictionary")

    cfg = ConfigParser()
    cfg.read(expanduser(filename))

    # This is a return dict
    insert(cfg, out, 'general', 'samplerate', int)
    insert(cfg, out, 'general', 'samplelength', int)
    insert(cfg, out, 'general', 'noise_amp', float)

    insert(cfg, out, 'samples', None, int)
    out['noise'] = {'test': cfg['noise']['test'].split(',')}

    insert(cfg, out, 'steps', 'slope', float)
    insert(cfg, out, 'steps', 'mode', str)

    insert(cfg, out, 'logsteps', 'slope', float)

    insert(cfg, out, 'inject', None, float)

    print("The function is not yet implemented")

    return out


def multinest(filename, out=None):
    """
    Read a config file for a multinest simulation. This will ensure that
    the types are what I expect them to be.
    """
    if out is None:
        out = {}
    elif type(out) != dict:
        raise Exception("The output needs to be a dictionary")

    cfg = ConfigParser()
    cfg.read(expanduser(filename))

    # This is a return dict
    insert(cfg, out, 'general', 'repeats', int)
    insert(cfg, out, 'general', 'monitor', bool)
    insert(cfg, out, 'general', 'samplerate', int)
    insert(cfg, out, 'general', 'samplelength', int)
    insert(cfg, out, 'general', 'fmin', float)
    insert(cfg, out, 'general', 'fmax', float)
    insert(cfg, out, 'general', 'noise_amp', float)

    insert(cfg, out, 'samples', None, int)
    out['noise'] = {
        'test': cfg['noise']['test'].split(','),
        'analysis': cfg['noise']['analysis'].split(','),
    }

    insert(cfg, out, 'steps', 'slope', float)

    insert(cfg, out, 'logsteps', 'slope', float)

    insert(cfg, out, 'inject', None, float)

    out['prior'] = {k: tuple(float(i) for i in v.split(','))
                    for k, v in cfg['prior'].items()}

    insert(cfg, out, 'multinest', 'n_live_points', int)
    insert(cfg, out, 'multinest', 'max_iter', int)
    insert(cfg, out, 'multinest', 'verbose', bool)
    insert(cfg, out, 'multinest', 'importance_nested_sampling', bool)
    insert(cfg, out, 'multinest', 'resume', bool)
    insert(cfg, out, 'multinest', 'sampling_efficiency', str)

    return out
