"""
This contains a class to analise noise properties from a given data set.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np


class Variance(object):
    """
    A variance class which calculates variance from a given data set.
    """
    def __init__(self, data, samplerate, skip=None, axis=0, fourier=True):
        """
        Calculate variance
        """
        self.axis = axis
        self.samplerate = samplerate

        if fourier:
            self.data = data
        else:
            raise Exception("Input data needs to be given in fourier domain")

        self.skip = skip

        if self.skip is not None:
            self.data = np.delete(self.data, self.skip, axis=self.axis)

        # FIXME axis should not be hardcoded!
        self.amps = np.abs(self.data).mean(axis=1)

        self.var = None
        self.psd = None

    def _chi_fit(self, amps):
        if self.skip is None:
            indices = np.arange(amps.shape[0])
        else:
            indices = np.arange(amps.shape[0] + len(self.skip))
            # Remove the missing sample x values
            indices = np.delete(indices, self.skip)

        A = np.vstack([indices, np.ones(indices.shape[0])]).T

        return np.linalg.lstsq(A, amps)[0]

    def chi_dedrift(self, mode="static"):
        """
        Remove any drift of the amplitude of the noise for calculating
        the PSD. This is crucial, as the theoretical framework needs the
        initial PSD and there is barely any other way to obtain this.

        There could be also a more elaborate fitting procedure. If we do
        not dedrift the amplitudes, then we will just overestimate the
        PSD, which is not good (need to investigate how bad it is).

        Parameters:
        -----------
        function: function
            A function name which could accept a numpy array as an argument.
            This is basically to assume a correct drift model.
        inverse: function
            This is another function, which is the inverse of the
            previous function. This also needs to accepts a numpy array.
        """
        if mode not in ["static", "logsteps", "steps"]:
            raise Exception("Dedrift is not implemented")

        if mode in ["steps", "logsteps"]:
            if mode == "steps":
                amps = self.amps
            elif mode == "logsteps":
                amps = np.log(self.amps)

            # Here we can use a given sampling to account for missing
            # samples along the line
            slope, initial = self._chi_fit(amps)

            for idx in range(amps.size):
                if mode == "steps":
                    factor = 1 / (1 + idx*slope/initial)
                elif mode == "logsteps":
                    factor = np.exp(-idx * slope)

                self.data[idx] *= factor

            print("Dedrifting details:")
            print("\t Slope: " + str(slope))
            print("\t Intercept: " + str(initial))

    def get(self):
        r"""
        Calculate variance ($\sigma^2$) in the Fourier domain. This is
        checking if we have a drifting noise and then acting
        accordingly.
        """
        bin_number = self.data.shape[1]

        # Calculate the observation time
        observation_time = bin_number / self.samplerate

        # Real and Imaginary will give independent estimates
        var_re = np.var(self.data.real, axis=self.axis)
        var_im = np.var(self.data.imag, axis=self.axis)

        # Taking average of imaginary and real parts to get a better estimate
        self.var = (var_re + var_im)/2
        # Make the variance symmetric, just in case it is not(due to
        # some rounding errors...)
        self.var = (self.var + self.var[::-1])/2

        # Select only the positive frequency components for psd output
        self.psd = 4 * self.var[bin_number/2:] / observation_time

        print("The mean values:")
        print("\t PSD: " + str(self.psd.mean()))
        print("\t Var: " + str(self.var.mean()))

        return self.var

    def save(self, filename_var, filename_psd):
        """
        Save the variance and the PSD to files.
        """
        print("Saving psd to: " + filename_psd)
        np.save(filename_psd, self.psd)

        print("Saving psd to: " + filename_var)
        np.save(filename_var, self.var)

        print("Done")
