"""
Probability calculation and inner products for a Gaussian distribution.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np
import bayes.util.array as hp

# Import the general class
from .general import Posterior


class Static(Posterior):
    """
    This class will calculate a probability for a given signal
    template and it will use the subsampling techniques in case we need
    to account for drifting noise. The formulae used for drifted noise
    case is already marginalised over the drift parameters.

    Parameters
    ----------
    data : array_like
        This is the data to fit
    var : array_like
        The Power Spectral Density function, for knowing the noise of the
        detector. Here we can use either real, or fake data
    tmp : array_like
        This is a signal template to fit. It should have the second dimension
        to be equal to data second dimension, but the first dimension needs to
        be the same as the list of interesting samples.
    searchin : list
        This is a list of numbers where we have the interesting samples to
        apply the template to, dimension needs to be the same as the first
        dimension of the template.
    """
    def __init__(self, data, template, variance, searchin, samplerate,
                 fmin=0, fmax=None):
        Posterior.__init__(self, data, template, variance, searchin, samplerate,
                           fmin, fmax)

        # Set the constant term self.alpha
        self.alpha = 0
        # Remove interesting samples before calculating constant vectors
        for sample in np.delete(self.data, self.searchin):
            # So this is the noise residual scaled by the variance
            self.alpha += hp.norm(sample, self.variance)

        print(self.alpha)

    def prob_slow(self, ndim, array=None, offset=0):
        r"""
        Return the probability function given some template.

        Returns $\ln p = - \frac{1}{2} \sum_{j, k=0}^{k=N-1}
        |n_k (f_j)|^2/\sigma^2_0(f_j)$

        Parameters:
        -----------
        template: array_like
            Fourier representation of a signal template to fit

        Returns:
        --------
        A logarithm of the probability
        """
        ret = 0

        for idx, sample in enumerate(self.data):
            if idx in self.searchin:
                if array is not None:
                    tmp = self.template.fourier(self.frequency, array)
                else:
                    tmp = self.template.fourier(self.frequency)

                prod = hp.norm(self.data[idx] - tmp, self.variance)
            else:
                prod = hp.norm(self.data[idx], self.variance)

            ret += prod

        return -0.5 * ret

    def prob(self, ndim, array=None, offset=0):
        r"""
        Return the probability function given some template.

        Returns $\ln p = - \frac{1}{2} \sum_{j, k=0}^{k=N-1}
        |n_k (f_j)|^2/\sigma^2_0(f_j)$

        Parameters:
        -----------
        template: array_like
            Fourier representation of a signal template to fit

        Returns:
        --------
        A logarithm of the probability
        """
        ret = self.alpha

        for idx in self.searchin:
            if array is not None:
                tmp = self.template.fourier(self.frequency, array)
            else:
                tmp = self.template.fourier(self.frequency)

            ret += hp.norm(self.data[idx] - tmp, self.variance)

        return -0.5 * ret


class LogDrift(Posterior):
    """
    A log drifting posterior, not marginalised over the variance
    """
    def __init__(self, data, Template, variance, searchin, samplerate,
                 fmin=0, fmax=None):
        Posterior.__init__(self, data, Template, variance, searchin, samplerate,
                           fmin, fmax)

        self.drift = 0

        # This is for non marginalised expression
        self.alpha = np.zeros(self.data.shape[0])
        self.beta = (self.alpha.size - 1) * (self.alpha.size - 2)\
            / 2 * self.frequency.size
        self.gamma = 2 * np.arange(self.alpha.size)

        # Remove interesting samples before calculating constant vectors
        for index, sample in enumerate(self.data):
            # Skip the interesting samples
            if index in self.searchin:
                continue

            # This is for non marginalised expression
            self.alpha[index] = hp.norm(sample, self.variance)

        print('Drift parameter: ' + str(self.drift))
        print('-'*83)

    def prob(self, ndim, array=None, offset=0):
        r"""
        This function will calculate a probability for a given signal
        template and it will use the subsampling techniques in case we need
        to account for drifting noise. The formulae used for drifted noise
        case is already marginalised over the drift parameters.

        The expression is:
        $p \propto
            \exp{(
                -\frac{(N-1)(N-2)}{2} \lambda
                - \sum_{k,j} \frac{|n_k(f_j)|^2}{2\sigma_1^2(f_j)} e^{-2k\lambda}
            )}$

        Parameters
        ----------
        tmp : array_like
            Signal template to fit
        drift : float
            The drift parameter
        """
        if array is not None and ndim > offset:
            drift = array[0 + offset]
        else:
            drift = self.drift

        # Assign a return array
        ret = self.alpha

        for index in self.searchin:
            if array is not None:
                tmp = self.template.fourier(self.frequency, array)
            else:
                tmp = self.template.fourier(self.frequency)
            ret[index] = hp.norm(self.data[index] - tmp, self.variance)

        # Weight the subsample results
        ret = ret * np.exp(-self.gamma * drift)

        # Sum over all frequencies
        return -0.5 * ret.sum() - self.beta * drift

    def _set_post_params(self, array, offset=0):
        """
        Set the drift parameter when calculating the probability.
        """
        self.drift = array[offset + 0]


class LogDriftMarginal(Static):
    """
    The marginalised expression for drifting noise
    """
    def __init__(self, data, Template, variance, searchin, samplerate,
                 fmin=0, fmax=None, invspread=0):
        Static.__init__(self, data, Template, variance, searchin, samplerate,
                        fmin, fmax)

        self.invspread = invspread

        self.beta = np.zeros(self.variance.size)
        self.gamma = np.zeros(self.variance.size)

        for index, sample in enumerate(self.data):
            # Skip if the index is interesting
            if index in self.searchin:
                continue

            # Calculate an element
            element = hp.norm_element(sample, self.variance)

            # Calculate constant vectors
            self.beta += index * element
            self.gamma += index**2 * element

        total = self.data.shape[0]

        self.beta -= (total-1) * (total-2) / 2

        print("Inverse of the spread of the lambda parameters: " +
              str(self.invspread))
        print('-'*83)

    def prob(self, ndim, array=None, offset=0):
        if array is not None and (ndim - 1 == offset):
            invspread = array[0 + offset]
        else:
            invspread = self.invspread

        alpha = self.alpha
        beta = self.beta
        # Use a Gaussian distribution of the spread
        gamma = self.gamma + 0.5 * invspread

        for index, sample in enumerate(self.data):
            if index not in self.searchin:
                continue

            # So this is the noise residual scaled by the variance
            if array is not None:
                tmp = self.template.fourier(self.frequency, array)
            else:
                tmp = self.template.fourier(self.frequency)

            product = hp.norm_element(sample - tmp, self.variance)

            # Carry out weighted sums
            alpha += product.sum()
            beta += index * product
            gamma += index**2 * product

        # Sum over all frequencies
        # b * (b/c) is more reliable than b²/c if b is large and it
        # is.
        return -0.5 * alpha + 0.25 * (beta * (beta/gamma) - 2 * np.log(gamma)).sum()
