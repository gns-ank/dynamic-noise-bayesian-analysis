"""
Multinest wrapper arround my probability library
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import subprocess
import os

import numpy as np

# Properly check, whether we can import pymultinest
try:
    import pymultinest
except ImportError as err:
    print(err)
    print("\n", '-'*30, 'Error', '-'*30)
    print("""PyMultiNest could not be loaded. Please check, whether it can be
    found in LD_LIBRARY_PATH""")
    print("Your LD_LIBRARY_PATH is " + os.environ['LD_LIBRARY_PATH'])
    import sys
    sys.exit(1)


class Likelihood(object):
    """
    A more abstract class
    """
    def __init__(self, Posterior, temp_prior, post_prior={}):
        """
        Initialiser, which takes a lot of parameters.

        Parameters
        ----------
        """
        # Class for the posterior generation
        self.posterior = Posterior

        # This is the number of parameters that go to construct the
        # template
        self.ntparams = len(temp_prior)

        # Convert the prior a, b values into a, b-a
        self.prior_length = np.zeros(self.ntparams + len(post_prior))
        self.prior_offset = np.zeros(self.ntparams + len(post_prior))
        self.param_keys = []
        self.n_params = 0

        def sorted_iterate(prior):
            # Construct the parameter members (we traverse the dict
            # alphabetically)
            for key, value in sorted(prior.items()):
                self.prior_length[self.n_params] = value[1] - value[0]
                self.prior_offset[self.n_params] = value[0]
                self.param_keys.append(key)
                self.n_params = len(self.param_keys)

        sorted_iterate(temp_prior)
        sorted_iterate(post_prior)

        print(Posterior.__class__.__name__)

    def _myprior(self, cube, ndim, nparams):
        """
        Prior function for the pymultinest calculator. Transform the unit
        parameter cube according to the parameter values
        """
        for i in range(ndim):
            cube[i] = cube[i] * self.prior_length[i] + self.prior_offset[i]

    def _myloglike(self, cube, ndim, nparams):
        """
        There must be a set_params function on the Signal and the
        Posterior object, which would accept the parameters as an array
        and the offset from where to read the parameters. It is crucial
        that functions have this interface, otherwise it won’t work.

            Class.set_params(array, offset)

        if offset is zero, it can be ommited.
        """
        debug = False

        if debug:
            # Calculate the probability via the slow pathway
            return self.posterior.prob_slow(ndim, cube, self.ntparams)
        else:
            # Calculate the probability
            return self.posterior.prob(ndim, cube, self.ntparams)

    def calc(self, filepath, **kwargs):
        """
        Calculate everything using pymultinest
        """
        multinest_parameters = {
            'max_iter': 0,
            'n_live_points': 1024,
            'importance_nested_sampling': False,
            'resume': False,
            'verbose': True,
            'sampling_efficiency': 'model',
        }

        # FIXME pymultinest should have a runtime config (or a section
        # of a config)
        multinest_parameters.update(kwargs)

        # Output_dir
        filepathdir = os.path.dirname(filepath)
        outputfiles_basename = os.path.join(filepathdir, u'1-')
        # progress_file = outputfiles_basename + 'phys_live.points.pdf'

        # we want to see some output while it is running
        if multinest_parameters['verbose']:
            progress = pymultinest.ProgressPrinter(
                n_params=self.n_params,
                interval_ms=30000,
                outputfiles_basename=outputfiles_basename
            )
            progress.start()
            # delayed opening
            # threading.Timer(2, show, [progress_file]).start()

        # run MultiNest
        pymultinest.run(
            self._myloglike, self._myprior, self.n_params,
            outputfiles_basename=outputfiles_basename,
            **multinest_parameters
        )

        if multinest_parameters['verbose']:
            # ok, done. Stop our progress watcher
            progress.stop()

    def analyse(self, filepath):
        """
        Analyse the multinest output using matplotlib and some stuff.
        """
        # Output_dir
        filepathdir = os.path.dirname(filepath)
        outputfiles_basename = os.path.join(filepathdir, u'1-')

        # lets analyse the results
        analyser = pymultinest.Analyzer(
            n_params=self.n_params,
            outputfiles_basename=outputfiles_basename
        )
        stats = analyser.get_stats()

        import json

        # store name of parameters, always useful
        with open('%sparams.json' % analyser.outputfiles_basename,
                  mode='w') as fout:
            json.dump(self.param_keys, fout, indent=2)

        # store derived stats
        with open('%sstats.json' % analyser.outputfiles_basename,
                  mode='w') as fout:
            json.dump(stats, fout, indent=2)

        print("\n", "-" * 30, 'ANALYSIS', "-" * 30)
        print("Global Evidence:\n\t%.15e +- %.15e" %
              (stats['nested sampling global log-evidence'],
               stats['nested sampling global log-evidence error']))

        import matplotlib.pyplot as plt
        plt.clf()

        # Here we will plot all the marginals and whatnot, just to show off
        # You may configure the format of the output here, or in matplotlibrc
        # All pymultinest does is filling in the data of the plot.

        # Copy and edit this file, and play with it.

        plot = pymultinest.PlotMarginalModes(analyser)
        plt.figure(figsize=(5*self.n_params, 5*self.n_params))

        # plt.subplots_adjust(wspace=0, hspace=0)
        for i in range(self.n_params):
            plt.subplot(self.n_params, self.n_params,
                        self.n_params * i + i + 1)
            plot.plot_marginal(i, with_ellipses=True, with_points=False,
                               grid_points=50)
            plt.ylabel("Probability")
            plt.xlabel(self.param_keys[i])

            for j in range(i):
                plt.subplot(self.n_params, self.n_params,
                            self.n_params * j + i + 1)
                plot.plot_conditional(i, j, with_ellipses=False,
                                      with_points=False, grid_points=80)
                plt.xlabel(self.param_keys[i])
                plt.ylabel(self.param_keys[j])

        plt.tight_layout()
        plt.savefig(filepath)  # ,bbox_inches='tight')

        for i in range(self.n_params):
            outfile = '%smode-marginal-%d.pdf' % \
                (analyser.outputfiles_basename, i)
            plot.plot_modes_marginal(i, with_ellipses=True, with_points=False)
            plt.ylabel("Probability")
            plt.xlabel(self.param_keys[i])
            plt.savefig(outfile, format='pdf', bbox_inches='tight')
            plt.close()

            outfile = '%smode-marginal-cumulative-%d.pdf' % \
                (analyser.outputfiles_basename, i)
            plot.plot_modes_marginal(i, cumulative=False, with_ellipses=True,
                                     with_points=True)
            plt.ylabel("Cumulative probability")
            plt.xlabel(self.param_keys[i])
            plt.savefig(outfile, format='pdf', bbox_inches='tight')
            plt.close()


def show(filepath):
    """ Edit this function to suit your needs """
    subprocess.call(('zathura', filepath))
