"""
A module, which defines the Posterior class and it’s interface
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np
import bayes.util.array as hp


class Posterior(object):
    """
    A base class for log posterior calculations
    """
    def __init__(self, data, Template, variance, searchin, samplerate,
                 fmin=0, fmax=None):
        """
        A comon initialisation for all.
        """
        self.data = data
        self.variance = variance
        self.searchin = searchin
        self.samplerate = samplerate
        self.frequency = hp.frequency(self.samplerate, self.data.shape[1])
        self.template = Template

        if fmax is None:
            fmax = self.samplerate / 2

        args = np.where((self.frequency >= fmin)
                        & (self.frequency < fmax))[0]

        # Use only frequencies in the range
        self.frequency = self.frequency[args[0]:args[-1]]
        self.data = self.data[:, args[0]:args[-1]]
        self.variance = self.variance[args[0]:args[-1]]

        # Feedback details of the class
        print('-'*30 + ' Initialised Posterior ' + '-'*30)
        print('Subsamples containing: ' + str(self.searchin))
        print('f_min, f_max: ' + str(fmin) + ', ' + str(fmax))
        print('Samplerate: ' + str(self.samplerate))
        print('Shape of the data array: ' + str(self.data.shape))

    def prob(self, array=None, offset=0):
        """
        This function is returning a probability when we pass it a
        template.
        """
        pass

    def _set_post_params(self, array, offset=0):
        """
        This function will set the parameters of the product. It will
        silently ignore if it is not doing anything.
        """
        pass

    def set_params(self, array, offset=0):
        """
        This sets the parameters of the posterior parameter space.
        This function is used to operate well with the Likelihood class.
        """
        self.template.set_params(array, 0)
        self._set_post_params(array, offset)
