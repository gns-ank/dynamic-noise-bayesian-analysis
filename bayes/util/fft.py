"""
At the moment the problem is that I have a notion of negative time in my
implementation and this screws things up. The DFT expects only positive time
and that is one of the reasons why I can not approximate ft with the DFT.  A
way to solve this would be to multiply with a complex phase
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np


def timeshift(array, index, **kwargs):
    """
    This function flips the order of the array for common fft operations

    Parameters:
    -----------
    array:  array_like
        The array to do the operation on
    pivot:  float
        This is the pivoting point. The default is -1, which means that the
        pivoting will be done in the middle of the array

    Returns:
    --------
    r:      array_like
        An array with a fliped order
    """
    axis = kwargs.get('axis', 0)

    if not len(array.shape) > axis:
        raise Exception("The chosen axis is invalid")

    N = array.shape[axis]

    # Only the modulo is needed
    index = np.floor(index) % N

    shift = np.exp(- 1j * 2 * np.pi * index * np.fft.fftfreq(N))
    if len(array.shape) > 1:
        shift = np.outer(np.ones(array.shape[:-1]), shift)

    # The actual sign depends on the convention used
    return array * shift


def fft(array, samplerate, pivot=0, **kwargs):
    """
    Do a fourier transform and have a consistent scalling across the whole
    software package. We return a sample in a normal orderred file.
    """
    axis = kwargs.get('axis', len(array.shape)-1)
    pivot = pivot*samplerate + samplerate*array.shape[axis]

    fft = np.fft.fft(array, axis=axis) / samplerate
    fft = timeshift(fft, pivot, axis=axis)

    return np.fft.fftshift(fft, axes=axis)


def ifft(array, samplerate, pivot=0, **kwargs):
    """
    Inverse Fourier Transform.
    """
    axis = kwargs.get('axis', len(array.shape)-1)
    pivot = pivot*samplerate + samplerate*array.shape[axis]

    ifft = np.fft.ifftshift(array, axes=axis)
    ifft = timeshift(ifft, -pivot, axis=axis)

    return np.fft.ifft(ifft, axis=axis) * samplerate
# }}}

# vim: foldmethod=marker:tw=80
