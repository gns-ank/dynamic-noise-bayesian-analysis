"""
A module to analyse the output filenames. Essentially, it's just plotting
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

# Matplotlip stuff
from matplotlib import rcParams

import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from os.path import splitext

import numpy as np

def psd(fname, fout, frequency, theory, **kwargs):
    """
    This is a function, which plots the psd into a separate file.

    Parameters:
    -----------

    fname: string
        This is a file name to open a psd output
    frequency: array_like
        Frequency to use to plot stuff
    theory: array_like
        The theoretical curve

    Returns:
    --------
    """
    fig = plt.figure()

    # Load a file
    psd = np.load(fname)

    # Use only positive frequencies
    frequency = frequency[psd.size:]
    theory = theory[psd.size:]

    # Do the plotting
    ax = fig.add_subplot(111)
    ax.plot(frequency, psd)
    ax.plot(frequency, theory)
    ax.set_xlabel(r'frequency $(f)/\mathrm{Hz}$')
    ax.set_ylabel(r'PSD $S(f)$')

    ax.set_title('The power spectral density')

    # Tight layout
    plt.tight_layout()

    plt.savefig(fout, **kwargs)


def signal(arg, n_types, data, t, fname, **kwargs):
    # Save the basename without the extension
    arg = splitext(arg)[0]

    fig = plt.figure()

    gs = gridspec.GridSpec(len(n_types), 1)
    ax = []

    for k, v in data.items():
        a = fig.add_subplot(gs[len(ax)])
        a.plot(t, v[0])
        if k != "clean":
            a.set_ylabel(r'$s(t) + n_{' + k + '}(t)$')
        else:
            a.set_ylabel(r'$s(t)$')
        ax.append(a)

    ax[0].set_title('The signal of the simulated wave packet')
    ax[-1].set_xlabel(r'Time $t$')

    plt.tight_layout()

    plt.savefig(arg + '-signal.png', **kwargs)


def prob_pcolorfast_gen(
        parameter_dims, parameter_names, infile, outfile, **kwargs):
    """
    A general function where len(pr_shape) >= 2. This will be really helpful if
    we want to select some particul directions(a.k.a. slices) in the
    multidimensional array.

    At the moment our function will just read the data and draw it.
    """
    # Read the data and select the required axis.
    data = mlab.csv2rec(infile)

    probability = data['posterior']

    # Find the list of elements non equal to 1
    idx = [i for i, e in enumerate(parameter_dims) if e != 1]

    if len(idx) != 2:
        raise Exception("The number of non zero axes is != 2. \
        Only 1d arrays work at the moment")

    labels = [parameter_names[i] for i in idx]

    parameters_one = data[labels[0]]
    parameters_two = data[labels[1]]

    # Now resize all the arrays
    # First, find the shape according to the parameter_dims list, which has all
    # the dimensions in the array.
    pr_shape = list(parameter_dims[i] for i in idx)
    # Second, reshape all of the arrays
    probability = probability.reshape(pr_shape[::-1])
    parameters_one = parameters_one.reshape(pr_shape[::-1])
    parameters_two = parameters_two.reshape(pr_shape[::-1])

    prob_pcolorfast(parameters_one, parameters_two, probability, labels,
                    outfile, **kwargs)

    return 0


def prob_contour_gen(
        parameter_dims, parameter_names, infile, outfile, **kwargs):
    """
    A general function where len(pr_shape) >= 2. This will be really helpful if
    we want to select some particul directions(a.k.a. slices) in the
    multidimensional array.

    At the moment our function will just read the data and draw it.
    """
    # Read the data and select the required axis.
    data = mlab.csv2rec(infile)

    probability = data['prob']

    # Find the list of elements non equal to 1
    idx = [i for i, e in enumerate(parameter_dims) if e != 1]

    if len(idx) != 2:
        raise Exception("The number of non zero axes is != 2. \
        Only 1d arrays work at the moment")

    parameters_one = data[parameter_names[idx[0]]]
    parameters_two = data[parameter_names[idx[1]]]

    # Now resize all the arrays
    # First, find the shape according to the parameter_dims list, which has all
    # the dimensions in the array.
    pr_shape = list(parameter_dims[i] for i in idx)
    # Second, reshape all of the arrays
    probability = probability.reshape(pr_shape)
    parameters_one = parameters_one.reshape(pr_shape)
    parameters_two = parameters_two.reshape(pr_shape)

    prob_contour(parameters_one, parameters_two, probability, outfile,
                 **kwargs)

    return 0


def prob_fun_gen(parameter_dims, parameter_names, infile, outfile, **kwargs):
    """
    A general function to plot many line graphs to see what is the probability
    distribution of parameters
    """
    # Read the data and select the required axis.
    data = mlab.csv2rec(infile)

    probability = data['prob']

    # Find the list of elements non equal to 1
    idx = [i for i, e in enumerate(parameter_dims) if e != 1]

    if len(idx) != 1:
        raise Exception("The number of non zero axes is != 1. \
        Only 1d arrays work at the moment")

    parameters = data[parameter_names[idx[0]]]

    prob_fun(parameters, probability, outfile, **kwargs)

    return 0


def prob_pcolorfast(x, y, z, labels, outfile, **kwargs):
    """

    """
    verbose = kwargs.get('verbose', False)

    if verbose:
        print("Plotting the probability surface")

    rcParams['xtick.direction'] = 'out'
    rcParams['ytick.direction'] = 'out'

    z = z[:-1, :-1]

    z_min = z.min()
    z_max = z.max()

    if verbose:
        print("Minimum and maximum probability values are: "
              + str(z_min) + ", " + str(z_max))

    # Create a simple contour plot with labels using default colors.  The
    # inline argument to clabel will control whether the labels are draw
    # over the line segments of the contour, removing the lines beneath
    # the label
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # Swap the stuff
    if np.all(y[0, :] == y):
        if verbose:
            print("Swapping the variables")
        x, y = y, x

    # Check what is the orientation of the arrays
    mpb = ax.pcolor(x, y, z, cmap='Blues', vmin=z_min, vmax=z_max)
    # mpb = ax.imshow(z, cmap='Blues', vmin=z_min, vmax=z_max)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    plt.axis([x.min(), x.max(), y.min(), y.max()])
    fig.colorbar(mpb)

    plt.title('Probability surface along of a parameter given the data')

    plt.tight_layout()
    plt.savefig(outfile, **kwargs)

    # Prevent a mem leak
    plt.close(fig)

    if verbose:
        print("The plot saved to file.\n")

    return 0


def prob_contour(X, Y, Z, outfile, **kwargs):
    """
    """
    rcParams['xtick.direction'] = 'out'
    rcParams['ytick.direction'] = 'out'

    # Create a simple contour plot with labels using default colors.  The
    # inline argument to clabel will control whether the labels are draw
    # over the line segments of the contour, removing the lines beneath
    # the label
    plt.figure()
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Probability surface along of a parameter given the data')

    plt.savefig(outfile, **kwargs)

    # Prevent a mem leak
    plt.close(CS)


def prob_fun(parameters, probability, outfile, **kwargs):
    """

    """
    # Create a simple line plot
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(parameters, probability)

    plt.title('Probability of the parameter given the data')

    plt.tight_layout()

    plt.savefig(outfile, **kwargs)

    # Prevent a mem leak
    plt.close(fig)
