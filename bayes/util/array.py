r"""
Array functions
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np


def inner_product_element(vector_a, vector_b, var):
    """
    A inner_product_element, where each component is a product of each
    component of a and b, weighted by the variance.
    """
    return vector_a * vector_b.conj() / var


def norm_element(vector, var):
    """
    A norm element, where each component is a product of each component
    of a and b, weighted by the variance.
    """
    return inner_product_element(vector, vector, var).real


def inner_product(vector_a, vector_b, var):
    """
    A frequency domain inner product between two vectors weighted by the
    variance.
    """
    return inner_product_element(vector_a, vector_b, var).sum()


def norm(vector, var):
    """
    A frequency domain norm of a vector weighted by the variance. The
    output is always real!!!
    """
    return inner_product(vector, vector, var).real


def freq2ids(samplerate, bins, fmin, fmax):
    """
    Freq_range to id_range
    """
    id1 = np.floor(fmin / samplerate * bins)
    id2 = np.ceil(fmax / samplerate * bins)

    return int(id1), int(id2) + 1

def time(samplerate, length, *numbers):
    """
    Parameters:
    -----------
    samplerate: int
        This is the sampling rate in Hz
    length: float
        This is the length of the subsample
    numbers: list
        This is the list of integers denoting samples taken to the [left,
        right, at] the region of interest.

    Returns: array_like
        Returns the time array
    """
    # The default value
    if len(numbers) < 2:
        numbers = [0, 0, 1]
    elif len(numbers) < 3:
        numbers.append(1)
    elif len(numbers) > 3:
        raise Exception("Too many parameters")

    # Calculate the number of sampling points
    total = sum(numbers)
    diff = numbers[1] - numbers[0]
    points = np.floor(length * samplerate) * total
    left = (diff - total) * length / 2
    right = (total + diff) * length / 2

    return np.linspace(left, right, points, endpoint=False)


def frequency(samplerate, bins):
    """
    the endpoint should not be included!!!
    """
    if int(bins) != np.floor(bins):
        raise Exception("The number of Fourier bins is not an integer")

    bins = int(bins)

    # Use numpy functions to generate only positive frequencies:
    f = samplerate * np.fft.fftfreq(bins)

    # Return frequency in normal order (i.e. starting with lowest)
    return np.fft.fftshift(f)
