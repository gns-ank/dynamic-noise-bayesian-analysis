r"""
Some utility functions, which ensure, that in other places in the code I am
always doing the same things when it comes to generating time or frequency
series.

This has only two functions and a constant:
    time generator
    frequency generator
    tau = $2 \pi$

I used the latter just because whenever there was np.pi, there was 2 as well.
So hp.tau made sense there. And only now I found that $\tau$ may indeed be the
right constant.
"""

import numpy

tau = 2 * numpy.pi

from . import array
from . import plot
from . import fft


def print_params(dictionary):
    """
    Print a nicely formated dictionary.
    """
    for key, value in dictionary.items():
        print("\t" + str(key) + ": " + str(value))
