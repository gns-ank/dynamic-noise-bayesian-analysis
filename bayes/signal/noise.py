"""
A noise generation module

This module should have all the necessary functions to generate and examine
noise.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np

import bayes.util.fft as hp
from .general import Noise


class LigoNoise(Noise):
    """
    Generate aLIGO like noise
    """
    def __init__(self, amplitude):
        self.amplitude = amplitude

        # Frequency parameters for the ALIGO PSD taken from a book
        self._cutoff = 20
        self._base = 215
        # Actual amplitude
        # self.amplitude = 1e-49

        self.printed = False

    def psd(self, frequency):
        """
        Return a ALIGO PSD function.

        Parameters
        ----------
        frequency : array_like
            frequency is a frequency array.

        Returns
        -------
        S : array_like
            This is a frequency domain array
        """
        # Use the general frequency generator
        frequency = np.abs(frequency)

        # Bellow the cutoff frequency, the noise does not increase
        frequency[np.where(frequency <= self._cutoff)] = self._cutoff

        # Scale the frequency
        arg = frequency/self._base

        form = np.power(arg, -4.14) - 5*arg**-2 \
            + 111 * (1 - arg**2 + 0.5 * arg**4) / (1 + 0.5 * arg**2)

        return self.amplitude * form

    def static_fourier(self, samplerate, bin_number):
        """
        This function generates a noise series.

        Parameters
        ----------

        Returns
        -------

        """
        # Calculate the psd for all frequencies
        frequency = np.linspace(0, samplerate/2, bin_number/2+1)

        # Observation time:
        observation_time = bin_number / samplerate

        # Now, calculate each component of imaginary and real part:
        n_f = np.array([], dtype=np.complex128)
        for idx, value in enumerate(self.psd(frequency)):
            a, b = np.random.normal(0, np.sqrt(value * observation_time)/2, 2)
            n_f = np.append(n_f, a + 1j*b)

        # We have the nyquist, negative and zero frequencies, hence,
        # assemble the array:
        n_f = np.append(n_f[::-1], n_f.conj()[1:-1])

        if n_f.size != bin_number:
            raise Exception("The noise generation algorithm is wrong")

        if not self.printed:
            print("The mean of the theoretical PSD")
            print("\t PSD: " + str(self.psd(frequency[:-1]).mean()))
            print("\t Var: " + str(0.25 * observation_time *
                                   self.psd(frequency[:-1]).mean()))
            self.printed = True

        return n_f

    def static(self, samplerate, bin_number):
        """
        Generate static purely Gaussian noise of given length and PSD.
        """
        # Calculate the frequency components:
        n_f = self.static_fourier(samplerate, bin_number)

        # Do inverse fourier transform with no pivoting
        return hp.ifft(n_f, samplerate).real


class Drift(object):
    def __init__(self, drift, mode):
        self.drift_amp = drift
        self.mode = mode

        self._allowed = ['static', 'steps', 'logsteps', 'smooth', 'logsmooth']

    def _linear_drift_fourier(self, num):
        """
        Linear drift of the noise in Fourier domain.  Each sample is
        just scaled by this function.
        """

        return 1 + np.linspace(0, 1, num, endpoint=False) * (self.drift_amp - 1)

    def _log_drift_fourier(self, num):
        return (self.drift_amp)**(np.linspace(0, 1, num, endpoint=True))

    def _log_drift_smooth(self, N, num):
        """
        N: int
            number of datapoints in a subsample
        """
        l = np.log(self.drift_amp) / (num - 1) / N
        fff = (N-1) * l / (np.exp((N-1)*l) - 1)

        return fff * np.exp(l * np.linspace(0, N*num, endpoint=False))

    def _linear_drift_smooth(self, N, num):
        factor = np.linspace(-0.5, num - 0.5, N*num, endpoint=False)

        return (1 + factor * (self.drift_amp - 1) / num)

    def drift_fourier(self, N, num):
        if self.mode == self._allowed[0]:
            drift = np.ones(num)
        elif self.mode == self._allowed[1]:
            drift = self._linear_drift_fourier(num)
        elif self.mode == self._allowed[2]:
            drift = self._log_drift_fourier(num)
        else:
            raise Exception("Invalid drift mode")

        return np.outer(drift, np.ones(N))

    def drift(self, N, num):
        if self.mode == self._allowed[0]:
            drift = np.zeros(num*N) + 1
        elif self.mode == self._allowed[1]:
            scale = self._linear_drift_fourier(num)
            length = np.ones(N)
            drift = np.outer(scale, length)
        elif self.mode == self._allowed[2]:
            scale = self._log_drift_fourier(num)
            length = np.ones(N)
            drift = np.outer(scale, length)
        elif self.mode == self._allowed[3]:
            drift = self._linear_drift_smooth(N, num)
        elif self.mode == self._allowed[4]:
            drift = self._log_drift_smooth(N, num)
        else:
            raise Exception("Invalid drift mode")

        return drift.flatten()


class NoiseSeries(LigoNoise, Drift):
    def __init__(self, amplitude, left, right, center, mode="static", drift=0):
        LigoNoise.__init__(self, amplitude)
        Drift.__init__(self, drift, mode)

        self.left = left
        self.right = right
        self.center = center
        self.total = self.left + self.center + self.right

    def fourier(self, samplerate, bin_number):
        # Allocate memory
        noise = np.zeros((self.total, bin_number), dtype=np.complex128)
        for i in range(self.total):
            # Drift the noise
            noise[i] = self.static_fourier(samplerate, bin_number)

        # The drift array
        drift = self.drift_fourier(bin_number, self.total)

        # Return the noise
        return noise * drift

    def time(self, samplerate, bin_number):
        r = self.static(samplerate, self.total * bin_number)

        return r * self.drift(bin_number, self.total)
