"""
The main signal template class
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

class Signal(object):
    """
    The signal template class, which returns various signal templates.
    """
    def __init__(self, *params, **kparams):
        pass

    def set_params(self, array, offset=0):
        """
        Set the parameters of the signal template from an array.
        """
        pass

    def time(self, time):
        """
        Return a template in time domain.
        """
        pass

    def fourier(self, frequency):
        """
        Return a template in frequency domain.
        """
        pass


class Noise(object):
    """
    The noise class, which returns various noise derived from a given PSD
    """
    def __init__(self):
        pass

    def psd(self, frequency, amplitude):
        pass
