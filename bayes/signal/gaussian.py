"""
This is an example class for signal templates. Having this is good, because it
makes some of the interaction between modules more standard/clear.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np

# A relative import
from .general import Signal


class Gaussian(Signal):
    """
    The signal template class, which returns various signal templates.
    """
    def __init__(self, *params, **kparams):
        if len(kparams) == 5:
            self.amplitude = kparams['amplitude']
            self.frequency = kparams['frequency']
            self.phase = kparams['phase']
            self.tcentral = kparams['tcentral']
            self.width = kparams['width']
        elif len(params) == 5:
            self.set_params(params)
        else:
            # Initialise the parameters to zero
            self.set_params(np.zeros(5))

    def set_params(self, array, offset=0):
        """
        Set the parameters of the signal template from an array.
        """
        self.amplitude = array[0+offset]
        self.frequency = array[1+offset]
        self.phase = array[2+offset]
        self.tcentral = array[3+offset]
        self.width = array[4+offset]

    def time(self, time):
        """
        Return a template in time domain.
        """
        time = time - self.tcentral
        tau = 2 * np.pi

        return self.amplitude \
            * np.sin(tau * self.frequency * time + self.phase * np.pi) \
            * np.exp(-0.5 * (time / self.width)**2)

    def fourier(self, frequency, params=None, offset=0):
        """
        Return a template in frequency domain.
        """
        if params is not None:
            amplitude = params[0+offset]
            frequency0 = params[1+offset]
            phase = params[2+offset]
            tcentral = params[3+offset]
            width = params[4+offset]
        else:
            amplitude = self.amplitude
            frequency0 = self.frequency
            phase = self.phase
            tcentral = self.tcentral
            width = self.width

        tau = 2 * np.pi

        # Calculate
        amplitude = amplitude * width * np.sqrt(tau)\
            * np.exp(-1j * tau * tcentral * frequency) / 2j
        gaussians = np.exp(
            1j * np.pi * phase
            - 0.5 * (tau * width * (frequency - frequency0))**2
            ) - np.exp(
            -1j * np.pi * phase
            - 0.5 * (tau * width * (frequency + frequency0))**2
            )

        return amplitude * gaussians


def data_fourier(signal, samplerate, length, *numbers, **kwargs):
    """
    This function is for generating data, it insert a single gaussian
    wavepacket of a fixed frequency. The noise is specified by the initial
    amplitude and a drift parameter. The noise is assumed to be pure Gaussian
    of a drifting variance.

    Parameters
    ----------
    params : dict of floats
        A dictionary of parameters,
    samplerate : double
        samples per second to generate
    length : double
        length in seconds of the sample

    Returns
    -------
    """
