"""
This is the main script runner.
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

from argparse import ArgumentParser
import os
import datetime
import itertools as it

import numpy as np

# We are using the pyfftw interface, as I do not want to waste CPU cycles on
# slow FFT in python.  However, once I know it is a bottleneck I will start to
# use it.
# import pyfftw as fft

# import my modules for doing this simulation
# The files are separated because I do not want to have everything in one file
import bayes.signal.gaussian as gen
import bayes.signal.noise as ns
import bayes.posterior as prb

import bayes.util as util
import bayes.config as bcfg


def gen_data(config, output_file):
    """
    Generate all types of data as an example.
    """
    # Parse the config
    samplerate = int(config['model']['samplerate'])
    samplelength = float(config['model']['length'])
    samples = [int(i) for i in config['model']['samples'].split(',')]

    # Convert some dictionaries
    params = {k: float(v) for k, v in config['params'].items()}
    noise = {k: float(v) for k, v in config['noise'].items()}

    # Write out all the noise types which we generate
    noise_types = ['clean', 'static', 'steps', 'linear']

    # Initiate time series which span number of samples to the left and right
    # around zero
    time = util.array.time(samplerate, samplelength, *samples)

    # Generate all data
    data = {key: gen.data(
        params, time, samplerate, samplelength, *samples, mode=key, **noise)
        for key in noise_types}

    # Create a filename saving mark
    mark = ''.join(['-' + str(i)
                    for i in [samplerate, samplelength] + samples])

    # Save the files
    for key, value in data.items():
        np.save(output_file + mark + '-' + key, value[0])
        np.save(output_file + mark + '-' + key + '-tr', value[1])

    np.save(output_file + mark + '-time', time)

    # Plot the data with a helper function
    util.plot.signal(output_file, noise_types, data, time,
               output_file + '-plot.png', dpi=200)


def experiment_pymultinest(output_dir, **kwargs):
    """
    PyMultinest executor for some parameters. This basically sets up the
    environment and executes things. Generate data and analyse it.
    """
    fnames_base = {'output': 'chains'}

    kwargs['start_counter'] = 0

    for i in range(kwargs['repeats']):
        # Generate file names
        fnames = {k: os.path.join(output_dir,
                                  v + '-' + str(i + kwargs['start_counter']))
                  for k, v in fnames_base.items()}

        # Create the directory if it doesn't exist, skip calculations if
        # it does
        if os.path.exists(fnames['output']):
            print('The folder exists, we are skipping this experiment')
            continue
        else:
            print("Creating dir: " + fnames['output'])
            os.makedirs(fnames['output'])

        print("Done: " + str(i) + " out of " + str(kwargs['repeats']))

        signal = gen.Gaussian(**kwargs['params_gen'])

        # Generate sample data
        bin_number = kwargs['samplerate'] * kwargs['samplelength']

        # create a signal packet by using the whole time array as it should be more
        # precise
        freq = util.array.frequency(kwargs['samplerate'], bin_number)

        noise = ns.NoiseSeries(
            kwargs['noise_amp'], *kwargs['samples'],
            mode=kwargs['noise_mode'], drift=kwargs['noise_data_drift_slope'])

        # Generate noise:
        data = noise.fourier(kwargs['samplerate'], bin_number)

        # Inject a signal into one of the noisy bits.
        data[kwargs['samples'][0]] += signal.fourier(freq)

        variance = prb.noise.Variance(
            data,
            kwargs['samplerate'],
            kwargs['searchin']
        )

        if kwargs["noise_analysis"] in ["logsteps", "logstepsmarginal"]:
            n_analysis = "logsteps"
        else:
            n_analysis = kwargs["noise_analysis"]

        variance.chi_dedrift(n_analysis)

        postparams = {}

        if kwargs['noise_analysis'] == 'static':
            posterior_fun = prb.gaussian.Static
        elif kwargs['noise_analysis'] in ['logsteps', "steps"]:
            posterior_fun = prb.gaussian.LogDrift
            postparams['drift'] = (-100, 100)
        elif kwargs['noise_analysis'] == 'logstepsmarginal':
            posterior_fun = prb.gaussian.LogDriftMarginal
        else:
            raise Exception("Unsuported noise model declared")

        # Limit the frequency in which we are carrying out the inner
        # products
        posterior = posterior_fun(
            data, signal, variance.get(), kwargs['searchin'], kwargs['samplerate'],
            kwargs['fmin'], kwargs['fmax']
        )

        print("The signal to noise ratio of the injected signal is:")
        print(util.array.norm(signal.fourier(posterior.frequency), posterior.variance))
        print()

        # Save the variance and the psd to file
        variance.save(os.path.join(fnames['output'], 'variance.npy'),
                      os.path.join(fnames['output'], 'psd.npy'))

        # Plot the psd
        util.plot.psd(os.path.join(fnames['output'], 'psd.npy'),
                      os.path.join(fnames['output'], 'psd-output.pdf'),
                      freq, noise.psd(freq))

        # continue

        # Create a likelihood object and generate a pdf
        fout = os.path.join(fnames['output'], 'triangle-plot.pdf')

        likelihood = prb.multinest.Likelihood(
            posterior, kwargs['params'], postparams
        )
        likelihood.calc(fout, **kwargs['multinest'])
        likelihood.analyse(fout)

        print("Done")


def experiment_pymultinest_series(output_dir, config_fname):
    """
    Loop over some values of pymultinest
    """

    config = bcfg.multinest(config_fname)

    cfg = config['general']
    cfg['params_gen'] = config['inject']
    cfg['params'] = config['prior']
    cfg['multinest'] = config['multinest']

    for analysis, generation in it.product(
            config['noise']['analysis'],
            config['noise']['test']):

        kwargs = cfg

        kwargs['noise_mode'] = generation
        if generation in ['steps', 'logsteps']:
            kwargs['noise_data_drift_slope'] = config[generation]['slope']
        else:
            kwargs['noise_data_drift_slope'] = 0
        kwargs['noise_analysis'] = analysis

        # Repeat if we have to
        totalnum = config['samples']['samplenumber']
        increment = config['samples']['increment']
        start = config['samples']['start']
        for i in range(start, totalnum, increment):

            kwargs['samples'] = [i, totalnum - 1 - i, 1]
            # kwargs['samples'] = [
            #     (totalnum - 1)//2, totalnum - 1 - (totalnum - 1)//2, 1]

            kwargs['searchin'] = [kwargs['samples'][0]]

            # Create a filename template
            base_name_tag = '-' + analysis + '-' + generation + '-' + \
                '-'.join(str(i) for i in kwargs['samples'])

            # Output to the user what we are doing
            print("Executing the experiment: "
                  + datetime.datetime.now().strftime("%x %X"))
            util.print_params(kwargs)

            # Run the experiment
            experiment_pymultinest(output_dir + base_name_tag, **kwargs)

    return 0


def get_arguments():
    """
    Our parameter parser executing the worker functions
    """
    parser = ArgumentParser(
        description="""
        This is a simulator as a part of my project for Part III Physics
        Research project.  At the moment it can do many things:

            1. Simulate data with different properties (no noise, simple noise,
            drifting noise)

            2. Analyse data of stationary noise by not accounting for many
        """)
    parser.add_argument(
        "-c", "--config", type=str,
        help="""
        Option file for any operation. This is needed to pass the initial
        parameters to the routine of choice.
        """)
    routines = parser.add_mutually_exclusive_group()
    routines.add_argument(
        "-d", "--data", type=str,
        help="""
        Generate data into a given filename. If the file does not exist, then
        generate the data and create the file. This is particularly useful for
        testing and reusing the data generated.
        """)
    parser.add_argument(
        "-b", "--basename", type=str,
        help="""
        A basename for saving files when doing various routines
        """)
    parser.add_argument(
        '-pm', '--pymultinest', action="store_true",
        help="""
        Whether to use pymultinest framework. It will not work if
        libmultinest.so is not in LD_LIBRARY_PATH!
        """)

    # Parse the arguments and error out if we do have unknown args
    return parser.parse_args()


def main():
    """
    The main function which runs argument parser and main functions when we
    call this as a script.
    """
    args = get_arguments()

    # Do the work
    if args.data and args.config:
        gen_data(args.config, args.data)
    elif args.pymultinest and args.basename and args.config:
        experiment_pymultinest_series(args.basename, args.config)
    else:
        print("Check your parameters. The combination of them was invalid.")

    return 0

if __name__ == "__main__":
    main()
