"""
Probability calculation and inner products for a student-t distribution
"""

from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import numpy as np
import bayes.util.array as hp

# Import the general class
from .general import Posterior


class LogDrift(Posterior):
    """
    A log drifting posterior, not marginalised over the variance
    """
    def __init__(self, data, variance, searchin):
        Posterior.__init__(self, data, variance)

        # Some default values for the extra parameters
        self.drift = 0
        self.degree_of_freedom = 1 << 20

        # Now, prepare the data:
        for index, sample in enumerate(self.data):
            # Skip the interesting samples
            if index in self.searchin:
                continue

            # Precalculate the norm elements.
            self.data[index] = hp.norm_element(sample, self.variance)

    def prob(self):
        r"""
        This function will calculate a probability for a given signal
        template and it will use the subsampling techniques in case we need
        to account for drifting noise. The formulae used for drifted noise
        case is already marginalised over the drift parameters.

        The expression is:
        $p \propto
            \exp{(
                -\frac{(N-1)(N-2)}{2} \lambda
                - \sum_{k,j} \frac{|n_k(f_j)|^2}{2\sigma_1^2(f_j)} e^{-2k\lambda}
            )}$

        Parameters
        ----------
        tmp : array_like
            Signal template to fit
        drift : float
            The drift parameter
        """

        # Assign a return array
        ret = np.zeros(self.data.shape[1])

        for index in self.searchin:
            tmp = self.template.fourier(self.frequency)
            ret += np.log
            data[index] = hp.norm(self.data[index] - tmp, self.variance)

        # Weight the subsample results
        ret *= np.exp(-2 * np.arange(ret.size) * self.drift)

        # Sum over all frequencies
        return -0.5 * (ret.sum() - (ret.size - 1) * (ret.size - 2) * self.drift)

    def _set_post_params(self, array, offset=0):
        """
        Set the drift parameter when calculating the probability.
        """
        self.drift = array[offset + 0]
        self.degree_of_freedom = array[offset + 1]
