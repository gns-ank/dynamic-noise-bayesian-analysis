"""
The probability calculator module.
"""

import numpy as np

from os.path import splitext
from string import Template

import bayes.util as hp
import bayes.signal.gaussian as gen
import bayes.posterior.noise as ns
import bayes.posterior.gaussian as prb


def calc_prob_array(posterior_fun, tmp_fun, freq, params, data, var,
                    interesting=[0], **kwargs):

    verbose = kwargs.get('verbose', False)

    # Create a recarray for the results of the simulation
    r = dict2recdata(params)

    drift = posterior_fun.__name__ == "log_posterior_drift"

    if drift:
        alpha, beta, gamma = prb.log_drift_marg_const(data, var, interesting)

    for i in range(r.size):
        if verbose and (i % 1000 == 0):
            print('Done: ' + str(i) + ' out of ' + str(r.size) + '.', end='\r')

        tmp = tmp_fun(freq, r[i])
        if drift:
            r['posterior'][i] = posterior_fun(data, var, tmp, interesting,
                                              alpha, beta, gamma)
        else:
            r['posterior'][i] = posterior_fun(data, var, tmp, interesting)

    # normalise the posterior (make max prob = 1), by subtracting from the log
    # prob
    r['posterior'] -= r['posterior'].max()

    return r


def slice_data(array, time, fs, length_subsample, subsamples, **kwargs):

    verbose = kwargs.get('verbose', False)

    diff = np.floor(fs*length_subsample)
    idx = {}

    idx['dataLow'] = np.where(time == -subsamples[2]*length_subsample/2)[0]
    idx['dataHigh'] = idx['dataLow'] + diff*subsamples[2]
    idx['noiseLow'] = idx['dataLow'] - diff*subsamples[0]
    idx['noiseHigh'] = idx['dataHigh'] + diff*subsamples[1]

    # Have all the values in integers
    idx = {k: int(v) for k, v in idx.items()}

    # Subsample the data with non overlapping samples
    data = array[idx['dataLow']:idx['dataHigh']]
    data = data.reshape((subsamples[2], -1))

    # Construct the noise array and calculate it's FFT
    noise = np.append(array[idx['noiseLow']:idx['dataLow']],
                      array[idx['dataHigh']:idx['noiseHigh']])
    noise = noise.reshape((subsamples[0] + subsamples[1], -1))

    if verbose:
        text = Template("""
    Indexes for arrays:
       Noise: $nl  \tData: $dl
              $nh  \t      $dh
    Time ranges of interest are:
        * Total:           $total
        * Contains signal: $signal
        * Contains noise:  $noise""")

        d = {'nl': str(idx[0]),
             'dl': str(idx[1]),
             'dh': str(idx[2]),
             'nh': str(idx[3]),
             'total': str(time[0]) + " to " + str(time[-1]),
             'signal': str(time[idx[1]]) + " to " + str(time[idx[2]]),
             'noise': str(time[idx[0]]) + " to " +
             str(time[idx[1]]) + " and from " +
             str(time[idx[2]]) + " to " + str(time[idx[3]])
             }
        print(text.substitute(d))

    return data, noise


def calc_posterior_simple(fnames, fs, length_subsample, subsamples, params,
                          **kwargs):

    verbose = kwargs.get('verbose', False)

    if verbose:
        print("Using the standard expression without subsampling and assuming \
                constant and known noise")

    # Load some numpy arrays into memory
    data = np.load(fnames['data'])
    time = np.load(fnames['time'])

    # Slice the data array
    data_t, noise_t = slice_data(data, time, fs, length_subsample, subsamples)

    # Fourier Transform the data and the noise
    data_ft = hp.fft.fft(data_t, fs, axis=1)
    noise_ft = hp.fft.fft(noise_t, fs, axis=1)

    return calc_posterior_simple_fourier(
        fnames['psd_out'], fnames['output'], fs, data_ft, noise_ft, params,
        **kwargs)


def calc_posterior_simple_fourier(
        fout_var, fout_prob, fs, data, noise, params,
        prob_fun, **kwargs):
    """
    This function is for analysing the data in the fourier domain.
    """
    verbose = kwargs.get('verbose', False)
    drift = kwargs.get('drift', False)
    fmin = float(kwargs.get('fmin', 0))
    fmax = float(kwargs.get('fmax', None))
    interesting = kwargs.get('interesting', [0])

    Nf = data.shape[1]

    if verbose:
        print("Calculating variance")

    var = ns.calc_var_fourier(noise, Nf, fs, fout_var, 0, drift=drift,
                              missing_samples=interesting)

    if fmax is None:
        fmax = fs/2

    freq = hp.array.frequency(fs, Nf)
    args = np.where((freq >= fmin) & (freq < fmax))[0]
    freq = freq[args[0]:args[-1]]

    # Use only frequencies in the range
    data = data[:, args[0]:args[-1]]
    var = var[args[0]:args[-1]]

    if verbose:
        print("Calculating probabilities")

    # Generate the probability
    record = calc_prob_array(
        prob_fun, gen.template_fourier, freq, params, data, var, interesting,
        verbose=verbose)

    idx = record['posterior'].argmax()

    if verbose:
        text = Template("""
    The best fit parameters are:
        Amplitude:      $amplitude
        Frequency:      $frequency
        Width:          $width
        Central time:   $tcentral
        Phase:          $phase\n""")

        # Generate results dictionary
        d = {k: record[k][idx] for k in params.keys()}

        print("Probability calculations done")
        print(text.substitute(d))

    # Output a recarray
    rec2csv(record, fout_prob)

    if verbose:
        print("Probability calculation results written to files")

    return record[idx]


# Modified version of a function found in:
#  http://stackoverflow.com/a/16409517/1105870
def ndmeshdict(dictionary):
    """
    Mesh dictionary
    """
    # Get the values from a dictionary
    args = map(np.asarray, list(dictionary.values()))

    # Generate views of the arrays
    value_views = np.broadcast_arrays(
        *[x[(slice(None),)+(None,)*i] for i, x in enumerate(args)]
        )

    # Return a different dictionary
    return dict(zip(list(dictionary.keys()), value_views))


def dict2recdata(dictionary):
    """
    Convert the dictionary to a record array, which we are using to iterate
    over the parameter space points.
    """
    meshdict = ndmeshdict(dictionary)

    dim = list(set([v.size for v in list(meshdict.values())]))

    if len(dim) != 1:
        raise Exception("The parameter arrays have different dimensions")

    for k, v in meshdict.items():
        meshdict[k] = v.flatten()

    dtype = {'names': list(meshdict.keys()) + ['posterior'],
             'formats': (len(meshdict) + 1) * [np.float64]}

    return np.rec.fromarrays(list(meshdict.values()) + [np.zeros(dim[0])],
                             dtype=dtype)


def rec2csv(array, fname):
    # Ensure that the fname is with a suitable extension
    fname = splitext(fname)[0] + '.csv'

    # Write everything into a file
    with open(fname, 'w') as fout:
        header = ','.join(array.dtype.names)
        fout.write(header + '\n')

        for i in array:
            line = ','.join([str(j) for j in i])
            fout.write(line + '\n')
