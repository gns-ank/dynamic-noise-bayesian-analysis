Driffting noise variance in Bayesian analysis
===

This is code for the Investigation of non-stationary noise in estimation of
gravitational wave parameters.

The libraries are implemented in Python 3 with maximum compatibility with the latest Python 2.7 versions.
Since modularity was kept in mind when designing the libraries, they are easily extendible and understandable.
Another side effect is that this separation makes it easy to test various portions of the code.
PEP8 conformance was checked in most of the places and as a result the code is consistent and tidy.

One can ensure that the output is correct by analysing output from the program, which can be of different verbosity depending on the command line switch passed to the program.

Dependencies
--

I assume that the following is available on the computer running the tool-kit:

NumPy
:   Without it there would be nothing sane, so this is essential. A recent
    version is recommended.

Python 3
:   I used the newer version of Python in order to make the lifetime of the
    code longer, but it should run with relatively few modification on a recent
    version of Python 2.7.

    I have used the necessary imports from `__future__` module and it may be
    the case that it will run with just changing the version of Python
    interpreter. However, beware, your mileage may vary.

PyMultiNest
:   This can be installed via `pip` and I advice on doing it this and not any
    other way (unless you really know what you are doing).

Multinest
:   There is a link to a repository on the PyMultiNest repository to a recent
    version of the MultiNest integrator. I assume that the compiled libraries
    of MultiNest are in `LD_LIBRARY_PATH`. You should compile them and copy
    them there or ask your system admin to do that.

Usage
--

The tool-kit was written in a modular way in order for it to be easily
extendible. The libraries are imported like any other Python libraries. Edit
the `dyno.py` file to change and set up new routines. In order to just change
parameters for the multinest or data generation routines, use the configuration
files provided in the `configs` directory. The special sub-module `configs` was
written in order to ensure that the types from the configuration files are
correct, so please use those functions. What is more, please use the mechanisms
to write new functions to parse configuration files as this will make my effort
more worth it and your life easier.

